const Table = require("cli-table");
const data = require("./data.json");

//filter data according to district and gender

const filt = (data, dist, gen) =>
  data.filter(
    obj => (obj["District"] === dist && obj["Gender"] === gen ? data : null)
  );

let gorkhaM = filt(data, "gorkha", "male");
let gorkhaF = filt(data, "gorkha", "female");
let gorkha = [...gorkhaM, ...gorkhaF];
let morangM = filt(data, "morang", "male");
let morangF = filt(data, "morang", "female");
let morang = [...morangM, ...morangF];
let nawalparasiM = filt(data, "nawalparasi", "male");
let nawalparasiF = filt(data, "nawalparasi", "female");
let nawalparasi = [...nawalparasiM, ...nawalparasiF];
let parsaM = filt(data, "parsa", "male");
let parsaF = filt(data, "parsa", "female");
let parsa = [...parsaM, ...parsaF];
let rasuwaM = filt(data, "rasuwa", "male");
let rasuwaF = filt(data, "rasuwa", "female");
let rasuwa = [...rasuwaM, ...rasuwaF];
let sindhupalchokM = filt(data, "sindhupalchok", "male");
let sindhupalchokF = filt(data, "sindhupalchok", "female");
let sindhupalchok = [...sindhupalchokM, ...sindhupalchokF];
let male = data.filter(m => m["Gender"] === "male");
let female = data.filter(m => m["Gender"] === "female");

let newArr = [];
const print = (dists, f, m) => {
  if (dists.length >= 90) {
    let x = ["Total", female.length, male.length, dists.length];
    newArr = [...newArr, x];
  } else {
    let y = [dists[0]["District"].replace(dists[0]["District"][0],dists[0]["District"][0].toUpperCase()), f.length, m.length, dists.length];
    newArr = [...newArr, y];
  }
  return newArr;
};

const table = new Table({
  head: ["District", "Female", "Male", "Total"],
  colWidths: [15, 10, 10, 10]
});

table.push(
  print(gorkha, gorkhaF, gorkhaM)[0],
  print(morang, morangF, morangM)[1],
  print(nawalparasi, nawalparasiF, nawalparasiM)[2],
  print(parsa, parsaF, parsaM)[3],
  print(rasuwa, rasuwaF, rasuwaM)[4],
  print(sindhupalchok, sindhupalchokF, sindhupalchokM)[5],
  print(data, female, male)[6]
);

console.log(table.toString());
