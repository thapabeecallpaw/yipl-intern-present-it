const fs = require("fs"),
  path = require("path"),
  csvtojson = require("csvtojson");

const json = [];

csvtojson()
  .fromFile("./data.csv")
  .on("json", jsonObj => {
    json.push(jsonObj);
  })
  .on("done", err => {
    if (err) {
      throw new Error("error");
    }
    fs.writeFile("data.json", JSON.stringify(json, null, 2), err => {
      console.log("end");
    });
  });
